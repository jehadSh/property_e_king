<?php

namespace App\Enums;


enum TypePropertyEnum: string
{
    case HOUSE = 'House';
    case FARM = 'Farm';
    case LAND = 'Land';
    case SHOP = 'Shop';
    case CHALET = 'Chalet';
    case INDUSTRIAL_FACILITY = 'Industrial_facility';
}
