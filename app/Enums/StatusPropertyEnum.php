<?php

namespace App\Enums;


enum StatusPropertyEnum: string
{
    case ENABLE = 'enable';
    case BLOCKED = 'blocked';
    case WAIT = 'wait';
}




