<?php

namespace App\Enums;


enum StatusOfficeEnum: string
{
    case ENABLE = 'enable';
    case BLOCKED = 'blocked';
    case WAIT = 'wait';
}




