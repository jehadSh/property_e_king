<?php

namespace App\Enums;


enum FileTypeEnum: string
{
    case UNIVERSITY_LIFE = 'university_life';
    case COLLEGE_DEGREE = 'college_degree';
    case SECONDARY_CERTIFICATE = 'secondary_certificate';
}
