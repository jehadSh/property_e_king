<?php

namespace App\Enums;


enum TypeContractEnum: string
{
    case SALE = 'sale';
    case RENT = 'rent';
}
