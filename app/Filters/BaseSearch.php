<?php

namespace App\Filters;

use Illuminate\Support\Arr;

class BaseSearch
{
    public function apply($query, $model, $prefix = '')
    {

        $filters = $this->getFilters($model->getFilterable(), $model->getModelRelations(), $prefix);
        if (isset($filters))
            foreach ($filters as $filter => $value) {
                if (is_array($value)) {
                    $newModel = new ($model->getModelRelations()[$filter]);
                    $query->withWhereHas($filter, function ($q) use ($newModel, $model, $filter, $prefix) {
                        $this->apply($q, $newModel, $prefix ? $prefix . $filter . '.' : $filter . '.');
                    });
                } else {
                    $filterInstance = new ($model->getFilterable()[$filter]);
                    $filterInstance($query, $filter, $value);
                }
            }
        return $query;
    }

    public function getFilters($filters, $relations, $prefix)
    {
        $data = request()->only(array_keys(Arr::prependKeysWith(array_merge($filters, $relations), $prefix)));
        if ($prefix != '') {
            $pre = explode('.', $prefix);
            $prefix = $pre[count($pre) - 2];
            $data = $this->findArrayByKey($data, $prefix);
        }
        return $data;
    }

    private function findArrayByKey(array $array, $keyToFind)
    {
        foreach ($array as $key => $value) {
            if ($key === $keyToFind) {
                return $value;
            }
            if (is_array($value)) {
                $result = $this->findArrayByKey($value, $keyToFind);
                if ($result !== null) {
                    return $result;
                }
            }
        }
        return null;
    }
}
