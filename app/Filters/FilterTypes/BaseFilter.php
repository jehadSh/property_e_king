<?php

namespace App\Filters\FilterTypes;

abstract class BaseFilter
{
    abstract function apply(\Illuminate\Database\Query\Builder $query, $field, $value);

}
