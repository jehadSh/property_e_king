<?php

namespace App\Filters\FilterTypes;

class TextFilter extends BaseFilter
{
    public function __invoke($query, $field, $value)
    {
        return $this->apply($query, $field, $value);
    }

    function apply($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }
}
