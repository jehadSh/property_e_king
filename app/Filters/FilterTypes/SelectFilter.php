<?php

namespace App\Filters\FilterTypes;

class SelectFilter extends BaseFilter
{
    public function __invoke($query, $field, $value)
    {
        return $this->apply($query, $field, $value);
    }

    function apply($query, $field, $value)
    {

        $ids = explode(",", $value);
        return $query->whereIn($field, $ids);
    }
}
