<?php

namespace App\Filters\FilterTypes;

class BooleanFilter extends BaseFilter
{
    public function __invoke($query, $field, $value)
    {
        return $this->apply($query, $field, $value);
    }

    function apply($query, $field, $value)
    {
        if (!is_bool((bool)$value)) {
            return $query;
        }
        return $query->where($field, (bool)$value);
    }
}
