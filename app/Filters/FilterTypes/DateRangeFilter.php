<?php

namespace App\Filters\FilterTypes;

use PhpParser\Builder;

class DateRangeFilter extends BaseFilter
{
    public function __invoke($query, $field, $value)
    {
        $this->apply($query, $field, $value);
    }

    function apply($query, $field, $value)
    {
        $arr = explode(',', $value);
        sort($arr);
        return $query
            ->whereDate($field, '>=', $arr[0])
            ->whereDate($field, '<=', $arr[1]);
    }
}
