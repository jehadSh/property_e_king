<?php

namespace App\Services\ModelsServices;

use App\DTOs\PropertyDTO;
use App\Models\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyService
{
    public function __construct(protected FileService $fileService)
    {
    }

    public function all()
    {
        return Property::with('files')->get();

    }

    public function index()
    {
        return Property::with('files')->filter()->paginate();
    }

    public function store(mixed $request, mixed ...$args): Property|array
    {
        $office = getArg('office', $args);

        $propertyDTO = PropertyDTO::fromRequest($request);
        $property = $office->properties()->create($propertyDTO->all());
        // dd($property);
        if (isset($request->files)) {
            $this->fileService->createManyFiles(
                $request->files,
                ['relatable' => $property, 'path' => 'property/' . $property->id . '/property/']
            );
        }
        return $property;
    }

    public function show(Model|Property $model)
    {
        return $model;
    }

    public function update(mixed $request, Model|Property $model, mixed ...$args): Property|array
    {
        $propertyDTO = PropertyDTO::fromRequest($request);
        $propertyDTO->mergeForUpdate($model);
        $model->update($propertyDTO->all());
        return $model;
    }

    public function destroy(Model|Property $model): void
    {
        $model->delete();
    }
}
