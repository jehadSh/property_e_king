<?php

namespace App\Services\ModelsServices;

use App\DTOs\FileDTO;
use App\Models\File;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class FileService
{

    public function all(): Collection
    {
        return File::paginate();
    }

    public function show(Model|File $model)
    {
        // Your implementation here
    }

    public function store(mixed $file, mixed ...$args): File|array
    {
        $model = getArg('relatable', $args);
        $path = getArg('path', $args);
        $type = getArg('type', $args);
        $path = storeFile($path, $file);
        $fileDTO = FileDTO::fromFile($file, $path, $type);
        $file = $model->files()->create($fileDTO->all());
        return $file;
    }

    public function createManyFiles(array $files, $args)
    {
        foreach ($files as $file) {
            $this->store($file['file'], $args);
        }
    }

    public function update(Request|FormRequest $request, Model|File $model, mixed ...$args): File|array
    {
        // Your implementation here
    }

    public function destroy(Model|File $model): void
    {
        // Your implementation here
    }

    public function index()
    {
        // TODO: Implement index() method.
    }
}
