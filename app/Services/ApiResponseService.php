<?php


namespace App\Services;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseService
{

    public static function successResponse($data = [], $msg = null, $code = 200): JsonResponse
    {
        return response()->json([
            'message' => $msg ?? trans('response.success'),
            'data'    => $data,
        ],
            $code);
    }


    public static function errorResponse($msg = null, $code = 400, $errors = null): JsonResponse
    {
        return response()->json([
            'message' => $msg,
            'errors'  => $errors ?? [trans('response.wrong')],
        ],
            $code);
    }


    /*****************************************************************************************/

    public static function validateResponse($errors, $code = 422): JsonResponse
    {
        return static::errorResponse(
            msg: $msg ?? trans('response.validation_error'),
            code: $code,
            errors: $errors,
        );
    }

    public static function successMsgResponse($msg = null, $code = 200)
    {
        return static::successResponse(
            msg: $msg ?? trans('response.success'),
            code: $code
        );
    }

    public static function errorMsgResponse($msg = null, $code = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return static::errorResponse(
            msg: $msg,
            code: $code
        );
    }

    public static function deletedResponse($msg = null, $code = 200): JsonResponse
    {
        return static::successResponse(
            msg: $msg ?? trans('response.deleted'),
            code: $code
        );
    }

    public static function createdResponse($data = [], $msg = null, $code = Response::HTTP_CREATED): JsonResponse
    {
        return static::successResponse(
            data: $data,
            msg: $msg ?? trans('response.created'),
            code: $code
        );
    }

    public static function updatedResponse($data = [], $msg = null, $code = Response::HTTP_CREATED): JsonResponse
    {
        return static::successResponse(
            data: $data,
            msg: $msg ?? trans('response.updated'),
            code: $code
        );
    }

    public static function notFoundResponse($msg = null, $code = Response::HTTP_NOT_FOUND): JsonResponse
    {
        return static::errorResponse(
            msg: $msg ?? trans('response.not_found'),
            code: $code
        );
    }

    public static function unauthorizedResponse($msg = null, $code = Response::HTTP_UNAUTHORIZED): JsonResponse
    {
        return static::errorResponse(
            msg: $msg ?? trans('response.unauthorized'),
            code: $code
        );
    }


}
