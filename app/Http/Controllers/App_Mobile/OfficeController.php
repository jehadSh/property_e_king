<?php

namespace App\Http\Controllers\App_Mobile;

;

use App\Http\Controllers\Controller;
use App\Models\Office;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class OfficeController extends Controller
{

    public function all()
    {
        $office = Office::all();
        return response()->json($office, 200);
    }

    public function index()
    {
        $office = Office::filter()->paginate(10);
        return response()->json($office, 200);
    }

    public function show($id)
    {
        $office = Office::where('id', $id)->get();
        return response()->json($office, 200);

    }


    public function update(Request $request, $id)
    {
        $office = Office::where('id', $id)->first();
        $office->update($request->all());

        return response()->json($office, 200);
    }

    public function destroy($id)
    {
        //
    }

    public function showMyProperty()
    {
        $office_id = Auth::user()->id;
        $myProperty = Property::where('office_id', $office_id)->get();
        return response()->json($myProperty, 200);
    }

    public function setOfficeRate(Request $request, $id)
    {
        $office = Office::where('id', $id)->first();
        $office->update([
            'rate' => ($office->rate + $request->rate) / 2,
        ]);
        return response()->json($office, 200);
    }

    public function showOfficeRate($id)
    {
        $office = Office::where('id', $id)->first();
        return response()->json($office->rate, 200);
    }
}
