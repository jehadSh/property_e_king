<?php

namespace App\Http\Controllers\App_Mobile;

use App\Http\Controllers\Controller;
use App\Models\Office;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $office = Office::create($request->all());
        $office->update([
            'password' => bcrypt($request->password),
        ]);
        $token = $office->createToken('my-app-token')->plainTextToken;
        $response = ['office' => $office, 'token' => $token];
        return response()->json($response, 201);
    }
    public function login(Request $request)
    {
        if ($request->phone == null || $request->password == null) {
            return response()->json('please enter your phone or your password', 201);
        }
        $fields = $request->validate([
            'phone' => 'required|string',
            'password' => 'required|string'
        ]);
        // Check email
        $office = Office::where('phone', $fields['phone'])->first();

        // Check password
        if (!$office || !Hash::check($fields['password'], $office->password)) {
            return response([
                'message' => 'Bad creds'
            ], 401);
        }

        $token = $office->createToken('my-app-token')->plainTextToken;

        $response = [
            'office' => $office,
            'token' => $token
        ];

        return response()->json($response, 201);
    }

    public function logout(Request $request)
    {
        Auth::user()->tokens()->delete();
        return response()->json('Logged out', 200);
    }

    // public function isToken(Request $request)
    // {
    //     $user = Auth::user();
    //     if ($user) {
    //         return response()->json($user, 200);
    //     } else {
    //         return response()->json('not login', 404);
    //     }

    // }

    // public function editPassword(Request $request)
    // {
    //     $user = Auth::user();
    //     $fields = $request->validate([
    //         'password' => 'required|string'
    //     ]);
    //     if (!$user || !Hash::check($fields['password'], $user->password)) {
    //         return response(['message' => 'كلمة السر خاطئة'], 401);
    //     }
    //     $user->update([
    //         'password' => bcrypt($request->newPassword),
    //     ]);
    //     return response()->json('Done ', 200);
    // }
}
