<?php

namespace App\Http\Controllers\App_Mobile;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePropertyRequest;
use App\Http\Requests\UpdatePropertyRequest;
use App\Http\Resources\PropertyResource;
use App\Models\Property;
use App\Services\ApiResponseService;
use App\Services\ModelsServices\PropertyService;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class PropertyController extends Controller
{
    public function __construct(
        protected PropertyService $propertyService,
    )
    {
    }

    public function all()
    {
        $property = $this->propertyService->all();
        return ApiResponseService::successResponse(PropertyResource::collection($property));
    }

    public function index()
    {
        $property = $this->propertyService->index();
        return ApiResponseService::successResponse(PropertyResource::collection($property));
    }

    public function store(CreatePropertyRequest $request)
    {
        $office = Auth::user();
        $property = $this->propertyService->store($request, ['office' => $office]);
        $property->loadMissing('files');
        return ApiResponseService::createdResponse(data: PropertyResource::make($property));
    }

    public function show(Property $property)
    {
        $property = $this->propertyService->show($property);
        $property->loadMissing('files');
        return ApiResponseService::successResponse(PropertyResource::make($property));
    }

    public function update(UpdatePropertyRequest $request, Property $property)
    {
        $property = $this->propertyService->update($request, $property);
        $property->loadMissing('files');
        return ApiResponseService::updatedResponse(PropertyResource::make($property));
    }

    public function destroy(Property $property)
    {
        $this->propertyService->destroy($property);
        return ApiResponseService::deletedResponse();
    }
}
