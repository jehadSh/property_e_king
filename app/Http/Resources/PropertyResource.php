<?php

namespace App\Http\Resources;

use App\Traits\ResourcePaginatorTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    use ResourcePaginatorTrait;

    public function toArray(Request $request): array
    {
        return [
            'id'            => $this->id,
            'office_id'     => $this->office_id,
            'type'          => $this->type,
            'floor'         => $this->floor,
            'space'         => $this->space,
            'room_counter'  => $this->room_counter,
            'direction'     => $this->direction,
            'price'         => $this->price,
            'property_type' => $this->property_type,
            'description'   => $this->description,
            'location'      => $this->location,
            'files'         => FileResource::collection($this->files),
        ];
    }
}
