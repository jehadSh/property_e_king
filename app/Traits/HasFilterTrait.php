<?php

namespace App\Traits;

use App\Filters\BaseSearch;



trait HasFilterTrait
{
    public function scopeFilter($query)
    {
        (new BaseSearch())->apply($query, new (self::class));
    }

    public function getFilterable()
    {
        return $this->filterable;
    }

    public function getModelRelations(): array
    {
        return $this->modelRelations;
    }

}
