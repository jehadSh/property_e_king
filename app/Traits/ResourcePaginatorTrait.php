<?php

namespace App\Traits;

trait ResourcePaginatorTrait
{

    public static function collection($data)
    {

        if (is_a($data, \Illuminate\Pagination\AbstractPaginator::class)) {
            $data->setCollection(
                $data->getCollection()->map(function ($listing) {
                    return new static($listing);
                })
            );
            return $data;
        }
        return parent::collection($data);
    }

}
