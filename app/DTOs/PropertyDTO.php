<?php

namespace App\DTOs;


use App\DTOs\ObjectData;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class PropertyDTO extends ObjectData
{
    static ?int $id;
    public ?string $office_id;
    public ?string $birth_date;
    public ?string $birth_place;
    public ?string $national_number;
    public ?string $card_number;
    public ?string $grant_source;
    public ?string $registration;
    public ?string $special_marks;
    public ?string $grant_date;

    public static function fromRequest($request): PropertyDTO
    {
        return new self([
            'office_id'     => $request->office_id ?? null,
            'type'          => $request->type ?? null,
            'floor'         => $request->floor ?? null,
            'space'         => $request->space ?? null,
            'room_counter'  => $request->room_counter ?? null,
            'direction'     => $request->direction ?? null,
            'price'         => $request->price ?? null,
            'property_type' => $request->property_type ?? null,
            'description'   => $request->description ?? null,
            'location'      => $request->location ?? null,
        ]);
    }
}
