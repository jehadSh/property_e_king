<?php

namespace App\DTOs;

use App\DTOs\ObjectData;
use Illuminate\Http\UploadedFile;

class FileDTO extends ObjectData
{
    public static ?int $id = null;
    public ?string $name;
    public ?string $path;
    public ?string $type;


    public static function  fromFile(UploadedFile $file, $path,$type)
    {
        return new self(
            [
                'name' => $file->getClientOriginalName(),
                'path' => $path,
                'type' => $type ?? null,
            ]
        );
    }

    public static function manyFromRequest(array $elements)
    {
        $objects = [];
        foreach ($elements as $element) {
            $object = new self(
                [
                    'name' => $element['name'],
                    'path' => $element['path'],
                    'type' => $element['type'] ?? null,
                ]
            );
            $objects[] = $object->all();
        }
        return $objects;
    }

}
