<?php

namespace App\DTOs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Request;
use Spatie\DataTransferObject\DataTransferObject;
use function PHPUnit\Framework\isNull;

abstract class ObjectData extends DataTransferObject
{
    public function mergeForUpdate(Model $model)
    {
        $DTO = $this;
        foreach ($DTO->all() as $key => $value) {
            if ($DTO->$key === null) {
                $DTO->$key = $model->$key;
            }
        }
        return $DTO;
    }

    public function allForUpdate()
    {
        $DTO = $this;
        $values = $DTO->all();
        $attrs = [];
        foreach ($values as $key => $value) {
            if (isset($value))
                $attrs[$key] = $value;
        }
        return $attrs;
    }

}
