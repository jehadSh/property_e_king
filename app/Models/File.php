<?php

namespace App\Models;

use App\Enums\FileTypeEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class File extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'name',
        'path',
        'type',
    ];

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'file' => [$required, 'mimes:pdf,png'],
//            'type' => [$required,  Rule::in(getEnumValues(FileTypeEnum::class))],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
