<?php

namespace App\Models;

use App\Filters\FilterTypes\SpecificTextFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;


class Office extends Model
{
    use HasFactory;
    use HasApiTokens;
    use HasFilterTrait;

    protected $fillable = [
        'name', 'phone', 'phone_office', 'otp',
        'location', 'activation_days', 'rate', 'status',
    ];
    protected $filterable = [
        'name'            => SpecificTextFilter::class,
        'phone'           => SpecificTextFilter::class,
        'phone_office'    => SpecificTextFilter::class,
        'location'        => TextFilter::class,
        'activation_days' => TextFilter::class,
        'rate'            => TextFilter::class,
        'status'          => SpecificTextFilter::class,
    ];

    public function getModelRelations(): array
    {
        return $this->modelRelations;
    }

    public $modelRelations = [
        'properties' => Property::class,

    ];

    public function properties(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Property::class, 'office_id');
    }
    public function files()
    {
        return $this->morphMany(File::class, 'relatable');
    }

}
