<?php

namespace App\Models;

use App\Filters\FilterTypes\SelectFilter;
use App\Filters\FilterTypes\SpecificTextFilter;
use App\Filters\FilterTypes\TextFilter;
use App\Traits\HasFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class Property extends Model
{
    use HasFactory;
    use HasFilterTrait;

    protected $fillable = [
        'type', 'floor', 'space', 'room_counter', 'direction', 'price',
        'property_type', 'description', 'location', 'office_id',
        'type_contract', 'status'
    ];

    protected $filterable = [
        'type'          => SpecificTextFilter::class,
        'floor'         => SpecificTextFilter::class,
        'space'         => TextFilter::class,
        'room_counter'  => SpecificTextFilter::class,
        'direction'     => SpecificTextFilter::class,
        'price'         => TextFilter::class,
        'property_type' => TextFilter::class,
        'description'   => TextFilter::class,
        'location'      => TextFilter::class,
        'office_id'     => SpecificTextFilter::class,
        'type_contract' => SpecificTextFilter::class,
        'status'        => SpecificTextFilter::class,
    ];

    public function getModelRelations(): array
    {
        return $this->modelRelations;
    }

    public $modelRelations = [
        'office' => Office::class,

    ];

    public function office()
    {
        return $this->belongsTo(Office::class, 'office_id');
    }

    public function files()
    {
        return $this->morphMany(File::class, 'relatable');
    }

    public static function rules(string $prefix = '', bool $is_nullable = false)
    {
        $required = $is_nullable ? 'nullable' : 'required';
        $rules = [
            'type' => [$required],
        ];
        return Arr::prependKeysWith($rules, $prefix);
    }
}
