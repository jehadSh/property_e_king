<?php

use App\Http\Controllers\App_Mobile\AuthController;
use App\Http\Controllers\App_Mobile\OfficeController;
use App\Http\Controllers\App_Mobile\PropertyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/register", [AuthController::class, 'register']);
Route::post("/login", [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post("/logout", [AuthController::class, 'logout']);
    Route::get("/isToken", [AuthController::class, 'isToken']);

    Route::prefix('property')->controller(PropertyController::class)->group(function () {
        Route::get('/', 'index');
        Route::get('/all', 'all');
        Route::post('create', 'store');
        Route::get('show/{property}', 'show');
        Route::put('update/{property}', 'update');
        Route::delete('delete/{property}', 'destroy');
    });
});


Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'office'], function () {
    Route::get("/", [OfficeController::class, 'index']);
    Route::get("/all", [OfficeController::class, 'all']);
    Route::get("show/{id}", [OfficeController::class, 'show']);
    Route::put("update/{id}", [OfficeController::class, 'update']);
    Route::get("show_my_property", [OfficeController::class, 'showMyProperty']);
    Route::get("set_office_rate/{id}", [OfficeController::class, 'setOfficeRate']);
    Route::get("show_office_rate/{id}", [OfficeController::class, 'showOfficeRate']);
});
