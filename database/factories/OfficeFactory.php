<?php

namespace Database\Factories;

use App\Enums\StatusOfficeEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfficeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'         => $this->faker->unique()->name,
            'phone'        => $this->faker->unique()->phoneNumber,
            'phone_office' => $this->faker->phoneNumber,
            'password'     => '$2y$10$ieErtiLR0yVGKIU6Qld5f.va/UxS2DensgK0e151Ufrb3amfTELKy',
            'otp'          => $this->faker->numberBetween($min = 0, $max = 6),
            'location'     => $this->faker->buildingNumber,
            'status'       => $this->faker->randomElement(getEnumValues(StatusOfficeEnum::class)),
        ];
    }
}
