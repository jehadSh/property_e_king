<?php

namespace Database\Factories;

use App\Enums\StatusPropertyEnum;
use App\Enums\TypeContractEnum;
use App\Enums\TypePropertyEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Mockery\Container;

class PropertyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type'          => $this->faker->randomElement(getEnumValues(TypePropertyEnum::class)),
            'floor'         => $this->faker->numberBetween($min = 0, $max = 10), // Generate a random integer between 0 and 10
            'space'         => $this->faker->numberBetween($min = 0, $max = 500), // Generate a random integer between 0 and 500
            'room_counter'  => $this->faker->numberBetween($min = 0, $max = 6),
            'direction'     => $this->faker->text,
            'price'         => $this->faker->buildingNumber,
            'property_type' => $this->faker->text,
            'description'   => $this->faker->sentence,
            'location'      => $this->faker->buildingNumber,
            'office_id'     => $this->faker->numberBetween($min = 1, $max = 2),
            'type_contract' => $this->faker->randomElement(getEnumValues(TypeContractEnum::class)),
            'status'        => $this->faker->randomElement(getEnumValues(StatusPropertyEnum::class)),
        ];
    }
}
