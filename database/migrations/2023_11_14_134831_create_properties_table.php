<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->enum('type', getEnumValues(\App\Enums\TypePropertyEnum::class));
            $table->tinyInteger('floor');
            $table->integer('space');
            $table->tinyInteger('room_counter');
            $table->string('direction');
            $table->unsignedBigInteger('price');
            $table->string('property_type');
            $table->longText('description');
            $table->string('location');
            $table->unsignedBigInteger('office_id');
            $table->enum('type_contract', getEnumValues(\App\Enums\TypeContractEnum::class));
            $table->enum('status', getEnumValues((\App\Enums\StatusPropertyEnum::class)));
            $table->timestamps();

            $table->foreign('office_id')->references('id')->on('offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign(['office_id']);
            $table->dropColumn('office_id');
        });
    }
}
