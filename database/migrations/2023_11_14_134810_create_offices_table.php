<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('phone')->unique();
            $table->string('phone_office');
            $table->string('password');
            $table->integer('otp')->nullable();
            $table->string('location')->nullable();
            $table->integer('activation_days')->nullable();
            $table->float('rate')->nullable();
            $table->enum('status', getEnumValues(\App\Enums\StatusOfficeEnum::class));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
